Feature('Login');


Scenario('Login com sucesso', ({ I }) => {
    
    I.amOnPage('http://automationpratice.com.br/');
        I.click('Login')
        I.waitForText('Login',10)
        I.click('#user')
        I.fillField('#user','rluanafreitas@gmail.com')
        I.click('#password')
        I.fillField('#password','casa1012')
        I.click('#btnLogin')
        I.waitForText('Login realizado',10)
       
}).tag('@Sucesso')


Scenario('Login com falha sem email', ({ I }) => {
        
    I.amOnPage('http://automationpratice.com.br/');
    I.click('Login')
    I.waitForText('Login',10)
    I.click('#user')
    I.fillField('#user','')
    I.click('#password')
    I.fillField('#password','casa1012')
    I.click('#btnLogin')
    I.waitForText('E-mail inválido.',10)

}).tag('@Falhaemail')


Scenario('Login com falha sem senha', ({ I }) => {

    I.amOnPage('http://automationpratice.com.br/');
    I.click('Login')
    I.waitForText('Login',10)
    I.click('#user')
    I.fillField('#user','rluanafreitas@gmail.com')
    I.click('#password')
    I.fillField('#password','')
    I.click('#btnLogin')
    I.waitForText('Senha inválida.',10)

}).tag('@Falhasenha')

Scenario('Tela do site da Aplee', ({ I }) => {

    I.amOnPage('https://www.apple.com/br/?afid=p238%7CsikJb1MHC-dc_mtid_1870765e38482_pcrid_630473557872_pgrid_17049243418_pntwk_g_pchan__pexid__&cid=aos-br-kwgo-brand--slid---product-');
    I.click('#ac-gn-link-search')
    I.fillField('#ac-gn-searchform-input','Iphone 14')
    I.pressKey('Enter');
    I.waitForText('iphone',10)
    I.click('https://www.apple.com/br/shop/buy-iphone/iphone-14-pro');
    
    
}).tag('@Aplee')